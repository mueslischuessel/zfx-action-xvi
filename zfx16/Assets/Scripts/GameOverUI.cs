﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverUI : MonoBehaviour {

    public Sprite[] digitSprites;
    public SpriteRenderer[] digits;
    public GameObject digitContainer;
    private ScoreManager scoreManager;
    public Vector3 originalDigitContainerPosition;

    // Use this for initialization
    void Start()
    {
        scoreManager = ScoreManager.instance;
        originalDigitContainerPosition = digitContainer.transform.localPosition;
        scoreManager.onScoreUpdatedEvent += ScoreChangedEvent;
        displayScore(scoreManager.Get());
    }

    private void ScoreChangedEvent(int score)
    {
        displayScore(score);
    }

    private void displayScore(int score)
    {
        // Align digitContainer
        int chars = (int)Math.Log10(score);
        digitContainer.transform.localPosition = new Vector3(
            originalDigitContainerPosition.x - (int)(11f - chars / 2.0f),
            originalDigitContainerPosition.y,
            originalDigitContainerPosition.z);

        for (int i = 0; i < digits.Length; i++)
        {
            digits[i].gameObject.SetActive(Math.Pow(10, i) <= score);
            digits[i].sprite = digitSprites[score % (int)Math.Pow(10, i + 1) / (int)Math.Pow(10, i)];
        }
    }
}
