﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour {

    public float onDelay = 0.4f;
    public float offDelay = 0.4f;

    private SpriteRenderer spriteRenderer;

    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();

        StartCoroutine(DoBlink());
	}
	
	IEnumerator DoBlink()
    {
        while (true)
        {
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(onDelay);

            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(offDelay);
        }
    }
}
