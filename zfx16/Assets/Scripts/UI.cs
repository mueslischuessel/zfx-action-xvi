﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    public GameObject[] hitpointObjects;
    public Sprite[] digitSprites;
    public SpriteRenderer[] digits;
    public HitpointController observingObject;
    private ScoreManager scoreManager;


	// Use this for initialization
	void Start () {
	    observingObject.onHealthChangedEvent += HealthChangedEvent;
        scoreManager =  ScoreManager.instance;
	    scoreManager.onScoreUpdatedEvent += ScoreChangedEvent;
	    displayScore(scoreManager.Get());

	}

    private void HealthChangedEvent(int hitpoints)
    {
        displayHitpoints(hitpoints);
    }

    private void ScoreChangedEvent(int score)
    {
        displayScore(score);
    }

    // Update is called once per frame
    void Update () {

	}

    private void displayHitpoints(int hitpoints)
    {
        for (int i = 0; i < hitpointObjects.Length; i++)
            hitpointObjects[i].SetActive(i + 1 <= hitpoints);
    }

    private void displayScore(int score)
    {
        for (int i = 0; i < digits.Length; i++)
        {
            digits[i].gameObject.SetActive(Math.Pow(10, i) <= score);
            digits[i].sprite = digitSprites[score % (int) Math.Pow(10, i + 1) / (int) Math.Pow(10, i)];
        }
    }
}
