﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HitpointController : MonoBehaviour
{
    public AudioClip hitSound;
    public AudioClip dyingSound;

    public int currentHitpoints = 3;
    public int maxHitpoints = 3;
    public float timeInvincibleAfterDamage = 3f;
    public float dyingTime = 1f;
    private bool invincible = false;
    private Animator animator;
    private AudioSource source;

    public event Action<int> onHealthChangedEvent;
    public event Action onDyingEvent;

    [Tooltip("Collisions Masks, this game object can collide.")]
    public LayerMask canBeHitByTheseLayers = 0;

    private BoxCollider2D collider;

    public enum state
    {
        Living,
        Dying,
    };


    // Use this for initialization
    void Start ()
    {
        collider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
    }

	// Update is called once per frame
	void Update () {

    }

    public void TakeDamage(int damage)
    {
        state oldState = GetState();
        if (invincible)
            return;

        if (source != null && hitSound != null)
        {
            source.PlayOneShot(hitSound);
        }

        currentHitpoints -= damage;
        currentHitpoints = Mathf.Clamp(currentHitpoints, 0, maxHitpoints);
        invincible = true;
        StartCoroutine(MakeVulnerable(timeInvincibleAfterDamage));
        if (oldState != state.Dying && GetState() == state.Dying)
        {
            if (source != null && dyingSound != null)
            {
                source.PlayOneShot(dyingSound);
            }

            if (onDyingEvent != null)
                onDyingEvent();

            Destroy(transform.gameObject, dyingTime);
        }
        updateAnimator();

        if (onHealthChangedEvent != null)
            onHealthChangedEvent(currentHitpoints);
    }

    public void TakeHeal(int heal)
    {
        currentHitpoints += heal;
        currentHitpoints = Mathf.Clamp(currentHitpoints, 0, maxHitpoints);
        updateAnimator();

        if (onHealthChangedEvent != null)
            onHealthChangedEvent(currentHitpoints);
    }

    public state GetState()
    {
        if (currentHitpoints > 0)
        {
            return state.Living;
        }
        else
        {
            return state.Dying;
        }
    }

    private IEnumerator MakeVulnerable(float time)
    {
        yield return new WaitForSeconds(time);
        invincible = false;
        updateAnimator();
    }

    private void updateAnimator()
    {
        if (animator.runtimeAnimatorController == null)
        {
            Debug.LogWarning("You need to an animation controller in order to update the animator state.");
            return;
        }

        animator.SetBool("alive", GetState() == state.Living);
        animator.SetBool("dying", GetState() == state.Dying);
        animator.SetBool("takingDamage", invincible);
    }

}
