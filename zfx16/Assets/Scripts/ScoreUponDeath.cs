﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(HitpointController))]
public class ScoreUponDeath : MonoBehaviour
{

    public int Score = 50;
    private ScoreManager scoreManager;

    // Use this for initialization
    void Start ()
	{
	    HitpointController hitpointController = GetComponent<HitpointController>();
        hitpointController.onDyingEvent += GiveScore;
        scoreManager = ScoreManager.instance;
	}

    private void GiveScore()
    {
        scoreManager.Add(Score);
    }

    // Update is called once per frame
	void Update () {

	}
}
