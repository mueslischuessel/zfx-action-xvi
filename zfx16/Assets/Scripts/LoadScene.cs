﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //MusicController.instance.SwitchToMenuScene();
    }

    // Update is called once per frame
    void Update () {
        if (Input.anyKeyDown)
        {
            MusicController.instance.SwitchToIngameScene();
            SceneManager.LoadScene("Scene");
        }
    }
}
