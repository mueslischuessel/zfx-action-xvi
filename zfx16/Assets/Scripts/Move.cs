﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public Vector3 perSecond;

    private void FixedUpdate()
    {
        transform.localPosition += perSecond * Time.fixedDeltaTime;
    }

}
