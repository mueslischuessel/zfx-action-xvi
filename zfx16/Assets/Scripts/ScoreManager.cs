﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    private ScoreManager boardScript;                       //Store a reference to our BoardManager which will set up the level.
    private int score = 0;

    public event Action<int> onScoreUpdatedEvent;

    //Awake is always called before any Start functions
    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);
    }

    void Update()
    {

    }

    public int Get()
    {
        return score;
    }


    public void Add(int score)
    {
        this.score += score;
        FireScoreUpdatedEvent();
    }

    public void Decrease(int score)
    {
        this.score += score;
        FireScoreUpdatedEvent();
    }

    public void Reset()
    {
        this.score = 0;
        FireScoreUpdatedEvent();
    }

    private void FireScoreUpdatedEvent()
    {
        if (onScoreUpdatedEvent != null)
            onScoreUpdatedEvent(score);
    }
}
