﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {

    public GameObject ship;
    public GameObject[] objectsToRemove;
    public GameObject gameOverUi;

    private HitpointController shipHitpoints;

	// Use this for initialization
	void Start () {
        shipHitpoints = ship.GetComponent<HitpointController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (shipHitpoints.currentHitpoints <= 0)
        {
            StartCoroutine(DoActivate());
        }
	}

    IEnumerator DoActivate()
    {
        yield return new WaitForSeconds(2.0f);

        foreach (GameObject obj in objectsToRemove)
        {
            Destroy(obj);
        }

        gameOverUi.SetActive(true);
    }
}
