﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrustMovement : MonoBehaviour
{
    public Vector3 velocity;
    private Rigidbody2D body;

    public GameObject thrusterGameObject;
    private ParticleSystem.EmissionModule thrusterEmitter;

    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        thrusterEmitter = thrusterGameObject.GetComponent<ParticleSystem>().emission;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        thrusterEmitter.enabled = Input.anyKey;

        if (Input.anyKey)
        {
            body.AddForce(velocity);
        }
    }
}
