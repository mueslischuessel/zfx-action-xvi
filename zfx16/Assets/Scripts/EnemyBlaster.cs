﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBlaster : MonoBehaviour {

    public GameObject bulletPrefab;
    public Boolean aimToPlayer;

    public float coolDown = 2f;
    private Transform player;

    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        foreach (var obj in GameObject.FindGameObjectsWithTag("Player"))
        {
            player = obj.transform;
            break;
        }

        source = GetComponent<AudioSource>();

        StartCoroutine(DoFire());
    }

    IEnumerator DoFire()
    {
        while (true)
        {
            source.Play();

            var bullet = Instantiate(bulletPrefab);
            Move move = bullet.GetComponent<Move>();
            // Aim if needed
            if (aimToPlayer && move != null && player != null)
            {
                float length = move.perSecond.magnitude;
                move.perSecond = player.position - transform.position;
                move.perSecond.Normalize();
                move.perSecond *= length;
            }

            bullet.transform.position = transform.position;
            yield return new WaitForSeconds(coolDown);
        }
    }
}
