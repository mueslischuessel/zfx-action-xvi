﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiledBackground : MonoBehaviour {

    private float verticalExtent;
    private float horizontalExtent;

    public GameObject[] tilePrefabs;

    private List<GameObject> availableTiles = new List<GameObject>();
    private List<GameObject> visibleTiles = new List<GameObject>();

    private int nextTile = 0;

    public float extensionBuffer = 20.0f;

	void Awake() {
        verticalExtent = Camera.main.GetComponent<Camera>().orthographicSize;
        horizontalExtent = verticalExtent * Screen.width / Screen.height;

        // Instantiate available tiles
        foreach (GameObject tilePrefab in tilePrefabs)
        {
            var newTile = Instantiate(tilePrefab);
            newTile.transform.parent = transform;
            availableTiles.Add(newTile);
        }

        FillVisibleTiles();

        StartCoroutine(DoAddAndRemove());
    }
	
    IEnumerator DoAddAndRemove()
    {
        while (true)
        {
            foreach (GameObject visibleTile in visibleTiles)
            {
                var collider = visibleTile.GetComponent<BoxCollider2D>();
                if ((collider.bounds.center + collider.bounds.extents).x < -horizontalExtent)
                {
                    availableTiles.Add(visibleTile);
                }
                else
                {
                    break;
                }
            }
            visibleTiles.RemoveAll(go => availableTiles.Contains(go));


            var rightBound = 0.0f;
            if (visibleTiles.Count > 0)
            {
                var collider = visibleTiles[visibleTiles.Count - 1].GetComponent<BoxCollider2D>();
                rightBound = (collider.bounds.center + collider.bounds.extents).x;
            }

            while (rightBound <= horizontalExtent + extensionBuffer)
            {
                var nextTile = availableTiles[Random.Range(0, availableTiles.Count - 1)];
                availableTiles.Remove(nextTile);

                nextTile.transform.parent = transform;
                nextTile.transform.localPosition = new Vector2(rightBound + nextTile.GetComponent<BoxCollider2D>().bounds.extents.x, 0.0f);
                nextTile.transform.localScale = Vector3.right * (Random.Range(0, 2) == 0 ? 1 : -1) + Vector3.up;
                visibleTiles.Add(nextTile);

                rightBound += nextTile.GetComponent<BoxCollider2D>().bounds.extents.x * 2.0f;
            }
            
            yield return new WaitForSeconds(1.0f);
        }
    }

    private void FillVisibleTiles()
    {
        var x = -horizontalExtent;
        while (x < 0.0f)
        {
            if (availableTiles.Count <= 0)
            {
                return;
            }

            var nextTile = availableTiles[Random.Range(0, availableTiles.Count - 1)];
            availableTiles.Remove(nextTile);

            nextTile.transform.localPosition = new Vector2(x, 0.0f);
            nextTile.transform.localScale = Vector3.right * (Random.Range(0, 2) == 0 ? 1 : -1) + Vector3.up;

            visibleTiles.Add(nextTile);

            x += nextTile.GetComponent<Collider2D>().bounds.size.x;
        }
    }

}
