﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveSpawner : MonoBehaviour
{
    public enum WavePhase
    {
        orchestrated,
        randomized
    }

    private WavePhase currentPhase = WavePhase.orchestrated;
    public WaveInfo[] waves;
    public WaveInfo[] randomWaves;
    public float initialPause = 5f;
    public float timeBetweenWaves = 1f;
    public bool isLooping = false;
    private bool pauseExceeded = false;
    private float waveTimeElapsed = 0f;
    private float PauseTimeElapsed = 0f;

    private int currentWave = 0;

	// Update is called once per frame
	void Update ()
	{
        // Wait for pause duration
	    if (!pauseExceeded)
	    {
	        PauseTimeElapsed += Time.deltaTime;
	        if (PauseTimeElapsed > initialPause)
	        {
	            pauseExceeded = true;
	            SpawnWave(waves[0]);
	        }
	        else
	        {
	            return;
	        }
	    }

	    if (currentPhase == WavePhase.orchestrated)
	    {
	        if (waveTimeElapsed > waves[currentWave].delay + timeBetweenWaves)
	        {
	            currentWave++;
	            waveTimeElapsed = 0;

	            if (currentWave < waves.Length)
	            {
	                SpawnWave(waves[currentWave]);
	            }
	            else
	            {
                    // Switch to randomized
                    currentPhase = WavePhase.randomized;
	                currentWave = Random.Range(0, randomWaves.Length);
	                SpawnWave(randomWaves[currentWave]);
                }
	        }
	        waveTimeElapsed += Time.deltaTime;
	    }
	    else if (currentPhase == WavePhase.randomized)
        {
	        if (waveTimeElapsed > randomWaves[currentWave].delay + timeBetweenWaves)
	        {
	            currentWave = Random.Range(0, randomWaves.Length);
	            waveTimeElapsed = 0;
	            SpawnWave(randomWaves[currentWave]);
	        }
	        waveTimeElapsed += Time.deltaTime;
        }

        // Gradually reduce the time
        timeBetweenWaves -= Time.deltaTime * 1f / 30f; // reduce 1 second every 30 seconds
	}

    void SpawnWave(WaveInfo wave)
    {
        Instantiate(wave.gameObject, transform);
    }
}
