﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blaster : MonoBehaviour {

    public GameObject bulletPrefab;

    public float coolDown = 0.2f;

    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();

        StartCoroutine(DoFire());
    }

    IEnumerator DoFire()
    {
        while (true)
        {
            if (Input.anyKey)
            {
                source.Play();

                var bullet = Instantiate(bulletPrefab);
                bullet.transform.position = transform.position;
            }

            yield return new WaitForSeconds(coolDown);
        }
    }
}
