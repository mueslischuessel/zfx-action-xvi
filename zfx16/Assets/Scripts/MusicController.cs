﻿using System.Collections;
using UnityEngine;

public class MusicController : MonoBehaviour {

    private static MusicController _instance;
    private float menuMusicVolume = 1f;
    private float InGameMusicVolume = 1f;
    private bool isSingleton = false;

    public static MusicController instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MusicController>();

                if (_instance.gameObject == null) // FIXME
                    return null;
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public GameObject menuMusicObject;
    private AudioSource menuMusic;

    public GameObject ingameMusicObject;
    private AudioSource ingameMusic;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(transform.gameObject);

        menuMusic = menuMusicObject.GetComponent<AudioSource>();
        ingameMusic = ingameMusicObject.GetComponent<AudioSource>();
    }

    void Awake()
    {
        // When loading menu anew, ensure, that not a duplicate instance is being created.
        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);
    }

    public void SwitchToIngameScene()
    {
        StartCoroutine(FadeMenuMusicOut());
        StartCoroutine(FadeIngameMusicIn());
    }

    public void SwitchToMenuScene()
    {
        StartCoroutine(FadeIngameMusicOut());
        StartCoroutine(FadeMenuMusicIn());
    }

    IEnumerator FadeMenuMusicOut()
    {
        for (int i = 0; i < 20; ++i)
        {
            menuMusic.volume = 1.0f - ((1.0f / 20.0f) * (float) i);
            yield return new WaitForSeconds(0.25f);
        }
        menuMusic.Stop();
    }

    IEnumerator FadeIngameMusicIn()
    {
        ingameMusic.Play();
        for (int i = 0; i < 20; ++i)
        {
            ingameMusic.volume = ((1.0f / 20.0f) * (float)i);
            yield return new WaitForSeconds(0.25f);
        }
    }

    IEnumerator FadeIngameMusicOut()
    {
        for (int i = 0; i < 20; ++i)
        {
            ingameMusic.volume = 1.0f - ((1.0f / 20.0f) * (float)i);
            yield return new WaitForSeconds(0.25f);
        }
        ingameMusic.Stop();
    }

    IEnumerator FadeMenuMusicIn()
    {
        menuMusic.Play();
        for (int i = 0; i < 20; ++i)
        {
            menuMusic.volume = ((1.0f / 20.0f) * (float)i);
            yield return new WaitForSeconds(0.25f);
        }
    }
}

