﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public int damage = 1;

    private BoxCollider2D collider;
    public LayerMask hitsTheseLayers = 0;
    public Boolean destroyAfterHit = false;

    // Use this for initialization
    void Start () {
	    collider = GetComponent<BoxCollider2D>();
    }

	// Update is called once per frame
	void Update () {

	}

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (isLayerInMask(hitsTheseLayers, col.gameObject.layer))
        {
            HitpointController enemyHitPoints = col.gameObject.GetComponent<HitpointController>();

            if (enemyHitPoints != null)
            {
                if (isLayerInMask(enemyHitPoints.canBeHitByTheseLayers, gameObject.layer))
                {
                    enemyHitPoints.TakeDamage(damage);

                }
            }
            if (destroyAfterHit)
                Destroy(gameObject);
        }
    }

    private bool isLayerInMask(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}
