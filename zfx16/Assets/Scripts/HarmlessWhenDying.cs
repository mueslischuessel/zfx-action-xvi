﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HitpointController))]
public class HarmlessWhenDying : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        HitpointController hitpointController = GetComponent<HitpointController>();
        hitpointController.onDyingEvent += RenderHarmless;
    }

    private void RenderHarmless()
    {
        EnemyBlaster blaster = GetComponentInChildren<EnemyBlaster>();
        if (blaster != null)
        {
            Destroy(blaster);
        }
        Bullet bullet = GetComponent<Bullet>();
        if (bullet != null)
        {
            Destroy(bullet);
        }

        BoxCollider2D collider = GetComponent<BoxCollider2D>(); // otherwise bullets would hit this dead enemy
        if (collider != null)
        {
            Destroy(collider);
        }

    }
}
