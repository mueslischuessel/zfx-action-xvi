﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour {

    public float delay = 1.0f;

    public float xPosition = -50f;

	void Start () {
        StartCoroutine(WaitAndDestroy());
	    StartCoroutine(CheckIfBelowXPosition());
	}

    IEnumerator WaitAndDestroy()
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }

    IEnumerator CheckIfBelowXPosition()
    {
        while (true)
        {
            if (transform.position.x <= xPosition)
            {
                Destroy(gameObject);
            }
            yield return null;
        }

    }

}
