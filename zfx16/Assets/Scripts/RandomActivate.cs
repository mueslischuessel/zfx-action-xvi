﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomActivate : MonoBehaviour {

    public float inactiveMinDelay = 5.0f;
    public float inactiveMaxDelay = 20.0f;

    public float activeMinDelay = 2.0f;
    public float activeMaxDelay = 5.0f;

    private ParticleSystem.EmissionModule emission;

	// Use this for initialization
	void Start () {
        emission = GetComponent<ParticleSystem>().emission;
        
        StartCoroutine(DoActivation());	
	}

    IEnumerator DoActivation()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(inactiveMinDelay, inactiveMaxDelay));
            emission.enabled = true;

            yield return new WaitForSeconds(Random.Range(activeMinDelay, activeMaxDelay));
            emission.enabled = false;
        }
    }

}
